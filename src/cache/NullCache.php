<?php


namespace tomdom\core\cache;


class NullCache implements cacheInterface
{
    public function set($name, $var, $exp = null)
    {

    }

    public function add($name, $var, $exp = null)
    {

    }

    public function getAllKeys()
    {
        return;
    }

    public function get($name, &$var2 = null, &$var3 = null)
    {
        return;
    }

    public function delete($name)
    {
        return;
    }

    public function addServer()
    {
        return false;
    }
}